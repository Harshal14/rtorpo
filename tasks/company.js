/*jslint node: true */
var request = require('request');
var apigee = require('../config.js');
var companies;
module.exports = function(grunt) {
	'use strict';
	grunt.registerTask('exportCompanies', 'Export all companies from org ' + apigee.from.org + " [" + apigee.from.version + "]", function() {
		var url = apigee.from.url;
		var org = apigee.from.org;
		var userid = apigee.from.userid;
		var passwd = apigee.from.passwd;
        var filepath = grunt.config.get("exportCompanies.dest.data");
		var done_count =0;
		var done = this.async();
		grunt.verbose.writeln("========================= export Companies ===========================" );

		grunt.verbose.writeln("getting companies..." + url);
		url = url + "/v1/organizations/" + org + "/companies";

		request(url, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				grunt.log.write("COMPANIES: " + body);
			    companies =  JSON.parse(body);
			    
			    if( companies.length == 0 ) {
			    	grunt.verbose.writeln("No Companies");
			    	done();
			    }
			    for (var i = 0; i < companies.length; i++) {
			    	var company_url = url + "/" + companies[i];
			    	grunt.file.mkdir(filepath);

			    	//Call company details
			    	grunt.verbose.writeln("COMPANY URL: " + company_url.length + " " + company_url);			    	
			    	if( company_url.length > 1024 ) {
			    		grunt.log.write("SKIPPING Company, URL too long: ");
			    		done_count++;
			    	} else {
						request(company_url, function (error, response, body) {
							if (!error && response.statusCode == 200) {
								grunt.verbose.writeln("Company " + body);
							    var company_detail =  JSON.parse(body);
							    var dev_file = filepath + "/" + company_detail.name;
							    grunt.file.write(dev_file, body);

							    grunt.verbose.writeln('Exported Company ' + company_detail.name);
							}
							else
							{
								grunt.verbose.writeln('Error Exporting Company ' + company_detail.name);
								grunt.log.error(error);
							}

							done_count++;
							if (done_count == companies.length)
							{
								grunt.log.ok('Processed ' + done_count + ' companies');
	                            grunt.verbose.writeln("================== export companies DONE()" );
								done();
							}
						}).auth(userid, passwd, true);
					}
			    	// End company details
			    };
			    
			} 
			else
			{
				grunt.log.error(error);
			}
		}).auth(userid, passwd, true);		
	});

	grunt.registerMultiTask('importCompanies', 'Import all companies to org ' + apigee.to.org + " [" + apigee.to.version + "]", function() {
		var url = apigee.to.url;
		var org = apigee.to.org;
		var userid = apigee.to.userid;
		var passwd = apigee.to.passwd;
		var files = this.filesSrc;
		var done_count=0;
		var opts = {flatten: false};
		var f = grunt.option('src');
		if (f)
		{
			grunt.verbose.writeln('src pattern = ' + f);
			files = grunt.file.expand(opts,f);
		}
		url = url + "/v1/organizations/" + org + "/companies";
		var done = this.async();

		files.forEach(function(filepath) {
			var content = grunt.file.read(filepath);			
			request.post({
			  headers: {'content-type' : 'application/json'},
			  url:     url,
			  body:    content
			}, function(error, response, body){
			  var status = 999;
			  if (response)	
				status = response.statusCode;
			  grunt.verbose.writeln('Resp [' + status + '] for company creation ' + this.url + ' -> ' +body);
			  if (error || status!=201)
			  { 
			  	grunt.verbose.error('ERROR Resp [' + status + '] for company creation ' + this.url + ' -> ' +body); 
			  }
			 done_count++;
			if (done_count == files.length)
			{
				grunt.log.ok('Processed ' + done_count + ' companies');
				done();
			}
			}.bind( {url: url}) ).auth(userid, passwd, true);

		});
	});

	grunt.registerMultiTask('deleteCompanies', 'Delete all companies from org ' + apigee.to.org + " [" + apigee.to.version + "]", function() {
		var url = apigee.to.url;
		var org = apigee.to.org;
		var userid = apigee.to.userid;
		var passwd = apigee.to.passwd;
		var files = this.filesSrc;
		var done_count = 0;
		var opts = {flatten: false};
		var f = grunt.option('src');
		if (f)
		{
			grunt.verbose.writeln('src pattern = ' + f);
			files = grunt.file.expand(opts,f);
		}
		url = url + "/v1/organizations/" + org + "/companies/";
		var done = this.async();
		files.forEach(function(filepath) {
			var content = grunt.file.read(filepath);
			var company = JSON.parse(content);
			var del_url = url + company.name;
			grunt.verbose.writeln(del_url);	
			request.del(del_url, function(error, response, body){
			  var status = 999;
			  if (response)	
				status = response.statusCode;
			  grunt.verbose.writeln('Resp [' + status + '] for company deletion ' + this.del_url + ' -> ' + body);
			  if (error || status!=200)
			  { 
			  	grunt.verbose.error('ERROR Resp [' + status + '] for company deletion ' + this.del_url + ' -> ' + body); 
			  }
				done_count++;
				if (done_count == files.length)
				{
					grunt.log.ok('Processed ' + done_count + ' companies');
					done();
				}
			}.bind( {del_url: del_url}) ).auth(userid, passwd, true);

		});
	});

};
