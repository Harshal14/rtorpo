/*jslint node: true */
var request = require('request');
var apigee = require('../config.js');
var async = require('async');
var caches;
module.exports = function(grunt) {
	'use strict';
	grunt.registerTask('exportEnvCache', 'Export all env-cache from org ' + apigee.from.org + " [" + apigee.from.version + "]", function() {
		var url = apigee.from.url;
		var org = apigee.from.org;
		var userid = apigee.from.userid;
		var passwd = apigee.from.passwd;
		var filepath = grunt.config.get("exportEnvCache.dest.data");
		var done_count =0;
		var done = this.async();
		var envs_url = url + "/v1/organizations/" + org + "/environments";
		var done = this.async();
		grunt.verbose.writeln("========================= export Env Cache's ===========================" );
	    grunt.verbose.writeln(envs_url);
		grunt.file.mkdir(filepath);
        
		request(envs_url, function (env_error, env_response, env_body) {
			if (!env_error && env_response.statusCode == 200) {
          grunt.verbose.writeln(env_body);
          var envs =  JSON.parse(env_body);
          
			    if( envs.length == 0 ) {
                    grunt.verbose.writeln ("exportEnvCache: No Cache's");
                    grunt.verbose.writeln("================== export cache DONE()" );
                    done();
                } else {
                	for (var j = 0; j < envs.length; j++) {
				    	var env = envs[j];
				    	var env_url = url + "/v1/organizations/" + org + "/environments/" + env + "/caches";
	                    
				    	grunt.verbose.writeln("Env Cache URL: " + env_url);
	                    
					    request(env_url, function (error, response, body) {
							if (!error && response.statusCode == 200) {
	                            grunt.verbose.writeln(body);
							    caches =  JSON.parse(body);   						    
							    for (var i = 0; i < caches.length; i++) {
							    	var env_cache_url = this.env_url + "/" + caches[i];	
							    	grunt.verbose.writeln("Cache URL : " + env_cache_url);
							    	var env = this.env;
	                                
							    	//Call cache details
									request(env_cache_url, function (error, response, body) {
										if (!error && response.statusCode == 200) {
											grunt.verbose.writeln(body);
										    var cache_detail =  JSON.parse(body);
										    var cache_file = filepath + "/" + this.env + "/" + cache_detail.name;
										    grunt.file.write(cache_file, body);
										}
										else
										{
											grunt.log.error(error);
										}
										done_count++;
										if (done_count == caches.length)
										{
											grunt.log.ok('Exported ' + done_count + ' caches');
                                            grunt.verbose.writeln("================== export ENV Cache DONE()" );
											done();
										}
	                                }.bind( {env: env})).auth(userid, passwd, true);
							    	// End cache details
							    };						    
							} 
							else
							{
								grunt.log.error(error);
							}
						}.bind( {env_url: env_url, env: env})).auth(userid, passwd, true);
					}
				}
			}
			else
			{
				grunt.log.error(error);
			}
		}).auth(userid, passwd, true);
	});

	grunt.registerMultiTask('importEnvCache', 'Import all env-cache to org ' + apigee.to.org + " [" + apigee.to.version + "]", function() {
		var url = apigee.to.url;
		var org = apigee.to.org;
		var userid = apigee.to.userid;
		var passwd = apigee.to.passwd;
		var done_count =0;
		var files;
		url = url + "/v1/organizations/" + org + "/";
		var done = this.async();
		var opts = {flatten: false};
		var f = grunt.option('src');
		if (f)
		{
			grunt.verbose.writeln('src pattern = ' + f);
			files = grunt.file.expand(opts,f);
		}
		else
		{
			files = this.filesSrc;
		}

		async.eachSeries(files, function (filepath,callback) {
			console.log(filepath);
			var folders = filepath.split("/");
			var env = folders[folders.length - 2];
			var content = grunt.file.read(filepath);
			var cache = JSON.parse(content);
			grunt.verbose.writeln("Creating Cache : " + cache.name + " under env " + env);
			grunt.verbose.writeln(JSON.stringify(cache));
			var cache_url = url + "environments/" + env + "/caches";
			grunt.verbose.writeln("Creating cache " + cache_url);

			request.post({
			  headers: {'Content-Type' : 'application/json'},
			  url:     cache_url,
			  body:    JSON.stringify(cache)
			}, function(error, response, body){
				try{
				  done_count++;
				  var cstatus = 999;
				  if (response)	
				  	  cstatus = response.statusCode;
				  if (cstatus == 200 || cstatus == 201)
				  {
				  grunt.verbose.writeln('Resp [' + response.statusCode + '] for create cache  ' + this.cache_url + ' -> ' + body);
				  var cache_resp = JSON.parse(body);
				  if (done_count == files.length)
					{
						grunt.log.ok('Processed ' + done_count + ' caches');
						done();
					}
					callback();
		      	  }
		      	  else
		      	  {
		      	  	grunt.verbose.writeln('ERROR Resp [' + response.statusCode + '] for create cache  ' + this.cache_url + ' -> ' + body);
		      	  	callback();
		      	  }
				}
				catch(err)
				{
					grunt.log.error("ERROR - from cache URL : " + cache_url );
					grunt.log.error(body);
				}

			}.bind( {cache_url: cache_url}) ).auth(userid, passwd, true);	
		});		
	});


	grunt.registerMultiTask('deleteEnvCache', 'Delete all env-cache from org ' + apigee.to.org + " [" + apigee.to.version + "]", function() {
		var url = apigee.to.url;
		var org = apigee.to.org;
		var userid = apigee.to.userid;
		var passwd = apigee.to.passwd;
		var done_count =0;
		var files;
		url = url + "/v1/organizations/" + org + "/";
		var done = this.async();
		var opts = {flatten: false};
		var f = grunt.option('src');
		if (f)
		{
			grunt.verbose.writeln('src pattern = ' + f);
			files = grunt.file.expand(opts,f);
		}
		else
		{
			files = this.filesSrc;
		}
		var done = this.async();
		files.forEach(function(filepath) {
			grunt.verbose.writeln("processing file " + filepath);
			var folders = filepath.split("/");
			var env = folders[folders.length - 2];
			var content = grunt.file.read(filepath);
			var cache = JSON.parse(content);
			var cache_del_url = url + "environments/" + env + "/caches/" + cache.name;
			grunt.verbose.writeln(cache_del_url);
			request.del(cache_del_url,function(error, response, body){
			   var status = 999;
			   if (response)	
			    status = response.statusCode;
			  grunt.verbose.writeln('Resp [' + status + '] for delete cache ' + this.cache_del_url + ' -> ' + body);
			  done_count++;
			  if (error || status!=200)
			  	grunt.verbose.error('ERROR Resp [' + status + '] for delete cache ' + this.cache_del_url + ' -> ' + body); 
			  if (done_count == files.length)
			  {
				grunt.log.ok('Processed ' + done_count + ' caches');
				done();
			  }
			}.bind( {cache_del_url: cache_del_url}) ).auth(userid, passwd, true);	
		});

	});

};
