/*jslint node: true */
var request = require('request');
var apigee = require('../config.js');
var users;
module.exports = function(grunt) {
	'use strict';
	grunt.registerTask('exportUsers', 'Export all users from org ' + apigee.from.org + " [" + apigee.from.version + "]", function() {
		var url = apigee.from.url;
		var org = apigee.from.org;
		var userid = apigee.from.userid;
		var passwd = apigee.from.passwd;
		var filepath = grunt.config.get("exportUsers.dest.data");
		var done_count =0;
		var done = this.async();
		grunt.verbose.writeln("========================= export Users ===========================" );

		grunt.verbose.writeln("getting users..." + url);
		url = url + "/v1/users";

		request(url, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				grunt.log.write("USERS: " + body);
			    users =  JSON.parse(body);			    
			    if( users.length == 0 ) {
			    	grunt.verbose.writeln("No Users");
			    	done();
                }
                
                var user_tag_detail = users.user;
			    for (var i = 0; i < user_tag_detail.length; i++) {
                    var users_url = url + "/" + user_tag_detail[i];
                    
			    	grunt.file.mkdir(filepath);

			    	//Call roles details
			    	grunt.verbose.writeln("USERS URL: " + users_url.length + " " + users_url);			    	
			    	if( users_url.length > 1024 ) {
			    		grunt.log.write("SKIPPING Users, URL too long: ");
			    		done_count++;
			    	} else {
						request(users_url, function (error, response, body) {
							if (!error && response.statusCode == 200) {
								grunt.verbose.writeln("USERS " + body);
							    var users_detail =  JSON.parse(body);
								var user_file = filepath + "/" + users_detail.name;								
								//if(roles_detail.name != 'devadmin' && roles_detail.name != null){
							    	grunt.file.write(user_file, body);
									grunt.verbose.writeln('Exported Roles ' + users_detail.name);
								//}
							}
							else
							{								
								grunt.log.error(error);
							}

							done_count++;
							if (done_count == users.length)
							{
								grunt.log.ok('Processed ' + done_count + ' users');
	                            grunt.verbose.writeln("================== export users DONE()" );
								done();
							}
						}).auth(userid, passwd, true);
					}
			    	// End roles details
			    };
			    
			} 
			else
			{
				grunt.log.error(error);
			}
		}).auth(userid, passwd, true);
	});

	grunt.registerMultiTask('importRoles', 'Import all roles  to org ' + apigee.to.org + " [" + apigee.to.version + "]", function() {
		var url = apigee.to.url;
		var org = apigee.to.org;
		var userid = apigee.to.userid;
		var passwd = apigee.to.passwd;
		var files = this.filesSrc;
		var done_count=0;
		var opts = {flatten: false};
		var f = grunt.option('src');
		if (f)
		{
			grunt.verbose.writeln('src pattern = ' + f);
			files = grunt.file.expand(opts,f);
		}
		url = url + "/v1/organizations/" + org + "/userroles";
		var done = this.async();

		files.forEach(function(filepath) {
			var content = grunt.file.read(filepath);
			//grunt.verbose.writeln(content);	
			request.post({
			  headers: {'content-type' : 'application/json'},
			  url:     url,
			  body:    content
			}, function(error, response, body){
			  var status = 999;
			  if (response)	
				status = response.statusCode;
			  grunt.verbose.writeln('Resp [' + status + '] for roles creation ' + this.url + ' -> ' +body);
			  if (error || status!=201)
			  { 
			  	grunt.verbose.error('ERROR Resp [' + status + '] for roles creation ' + this.url + ' -> ' +body); 
			  }
			 done_count++;
			if (done_count == files.length)
			{
				grunt.log.ok('Processed ' + done_count + ' roles');
				done();
			}
			}.bind( {url: url}) ).auth(userid, passwd, true);

		});
	});

	grunt.registerMultiTask('deleteRoles', 'Delete all roles from org ' + apigee.to.org + " [" + apigee.to.version + "]", function() {
		var url = apigee.to.url;
		var org = apigee.to.org;
		var userid = apigee.to.userid;
		var passwd = apigee.to.passwd;
		var files = this.filesSrc;
		var done_count = 0;
		var opts = {flatten: false};
		var f = grunt.option('src');
		if (f)
		{
			grunt.verbose.writeln('src pattern = ' + f);
			files = grunt.file.expand(opts,f);
		}
		url = url + "/v1/organizations/" + org + "/userroles/";
		var done = this.async();
		files.forEach(function(filepath) {
			var content = grunt.file.read(filepath);
			var role = JSON.parse(content);
			var del_url = url + role.name;
			grunt.verbose.writeln(del_url);	
			request.del(del_url, function(error, response, body){
			  var status = 999;
			  if (response)	
				status = response.statusCode;
			  grunt.verbose.writeln('Resp [' + status + '] for role deletion ' + this.del_url + ' -> ' + body);
			  if (error || status!=200)
			  { 
			  	grunt.verbose.error('ERROR Resp [' + status + '] for role deletion ' + this.del_url + ' -> ' + body); 
			  }
				done_count++;
				if (done_count == files.length)
				{
					grunt.log.ok('Processed ' + done_count + ' roles');
					done();
				}
			}.bind( {del_url: del_url}) ).auth(userid, passwd, true);

		});
	});

};
