/*jslint node: true */
var request = require('request');
var apigee = require('../config.js');
var async = require('async');
var devs;
// require('request-debug')(request);
module.exports = function (grunt) {
	'use strict';
	grunt.registerTask('exportCompanyDevs', 'Export all company apps from org ' + apigee.from.org, function () {
		var url = apigee.from.url;
		var org = apigee.from.org;
		var userid = apigee.from.userid;
		var passwd = apigee.from.passwd;
		var filepath = grunt.config.get("exportCompanyDevs.dest.data");
		var done_count = 0;
		var company_count = 0;
		var total_devs = 0;
		var company_url;
		var done = this.async();
		grunt.verbose.writeln("========================= export Developers ===========================");
		grunt.verbose.writeln("getting companies..." + url);
		url = url + "/v1/organizations/" + org + "/companies";

		var dumpDevs = function (name) {
			company_url = url + "/" + name;
			//Call company details
			console.log('company_url:'+company_url)

			request(company_url, function (company_error, company_response, company_body) {
				if (!company_error && company_response.statusCode == 200) {
					var company_detail = JSON.parse(company_body);
					var last = company_detail.name;
					var company_folder = filepath + "/" + company_detail.name;
                    grunt.file.mkdir(company_folder);

					var devs = company_detail.devs;

                    //Get company developers
                    var devs_url = url + "/" + company_detail.name + "/developers?expand=true";
                    grunt.verbose.writeln(devs_url);
                    request(devs_url, function (dev_error, dev_response, dev_body) {
                        if (!dev_error && dev_response.statusCode == 200) {

                            var devs_detail = JSON.parse(dev_body);
                            grunt.verbose.writeln(dev_body);                                                
                            var devs = devs_detail.developer;
                            grunt.verbose.writeln(devs);                                                
                            if (devs) {
                                for (var j = 0; j < devs.length; j++) {
                                    var developer = devs[j];
                                    grunt.verbose.writeln(JSON.stringify(developer));
                                    var file_name = company_folder + "/" + developer.email;                                                        
                                    grunt.verbose.writeln("writing file: " + file_name);
                                    grunt.file.write(file_name, JSON.stringify(developer));
                                    grunt.verbose.writeln('Company Dev ' + developer.email + ' written!');
                                };
                            }
                            total_devs += devs.length;

                            if (devs.length > 0) {
                                grunt.log.ok('Retrieved ' + devs.length + ' company devs for ' + company_detail.name);
                            } else {
                                grunt.verbose.writeln('Retrieved ' + devs.length + ' company devs for ' + company_detail.name);
                            }
                        }
                        else {
                            grunt.verbose.writeln('Error Exporting ' + devs.name);
                            grunt.log.error(body);
                        }
                        done_count++;
                        if (done_count == company_count) {
                            grunt.log.ok('Exported ' + total_devs + ' company devs for ' + done_count + ' developers');
                            grunt.verbose.writeln("================== export company devs DONE()" );
                            done();
                        }
                    }).auth(userid, passwd, true);
                    // End Company Dev details
				}
			}).auth(userid, passwd, true);
			// End Company details
		}

		var iterateOverCompanies = function (start, base_url, callback) {
			var url = base_url;

			if (start) {
				url += "?startKey=" + encodeURIComponent(start);
			}
			grunt.verbose.writeln("getting companies..." + url);

			request(url, function (error, response, body) {
				if (!error && response.statusCode == 200) {
					var companies = JSON.parse(body);
					var last = null;

					// detect none or that the only company returned is the one we asked to start with; that's the end game.
					if ((companies.length == 0) || (companies.length == 1 && companies[0] == start)) {
						grunt.log.ok('Retrieved total of ' + company_count + ' companies');
						// done();
					} else {
						company_count += companies.length;
						if (start)
							company_count--;

						for (var i = 0; i < companies.length; i++) {
							// If there was a 'start', don't do it again, because it was processed in the previous callback.
							if (!start || companies[i] != start) {
								callback(companies[i]);
								last = companies[i];
							}
						}

						grunt.log.ok('Retrieved ' + companies.length + ' companies');

						// Keep on calling getcompanies() as long as we're getting new companies back
						iterateOverCompanies(last, base_url, callback);
					}
				}
				else {
					if (error)
						grunt.log.error(error);
					else
						grunt.log.error(body);
				}

			}).auth(userid, passwd, true);
		}

		iterateOverCompanies(null, url, dumpDevs);
	});


	grunt.registerMultiTask('importCompanyDevs', 'Import all company devs to org ' + apigee.to.org, function () {
		var url = apigee.to.url;
		var org = apigee.to.org;
		var userid = apigee.to.userid;
		var passwd = apigee.to.passwd;
		var done_count = 0;
		var files;
		url = url + "/v1/organizations/" + org + "/developers/";
		var opts = {
			flatten: false
		};
		var f = grunt.option('src');
		if (f) {
			grunt.verbose.writeln('src pattern = ' + f);
			files = grunt.file.expand(opts, f);
		} else {
			files = this.filesSrc;
		}

		var done = this.async();

		async.eachSeries(files, function (filepath, callback) {
			console.log(filepath);
			var folders = filepath.split("/");
			var dev = folders[folders.length - 2];
			var content = grunt.file.read(filepath);
			var developer = JSON.parse(content);
			grunt.verbose.writeln("Creating developer : " + developer.email + " under developer " + developer);

			var status_done = false;
			var delete_done = false;

			delete developer['email'];
			//delete app['status'];
			delete developer['role'];
			//delete app['lastModifiedAt'];
			//delete app['lastModifiedBy'];
			//delete app['createdAt'];
			//delete app['createdBy'];
			//delete app['status'];
			//delete app['appFamily'];
			//delete app['accessType'];
			//delete app['credentials'];

			grunt.verbose.writeln(JSON.stringify(developer));
			var developer_url = url + developer + "/developers";
			grunt.verbose.writeln("Creating Developer " + developer_url);

			request.post({
				headers: {
					'Content-Type': 'application/json'
				},
				url: developer_url,
				body: JSON.stringify(developer)
			},
				function (error, response, body) {
					try {
						var cstatus = 999;
						if (response)
							cstatus = response.statusCode;
						if (cstatus == 200 || cstatus == 201) {
							grunt.verbose.writeln('Resp [' + response.statusCode + '] for create developer  ' + this.developer_url + ' -> ' + body);
							var dev_resp = JSON.parse(body);

							// Set app status
							if (developer['status'] && (dev_resp['status'] != developer['status'])) {
								var status_url = developer_url + '/' + developer.email + '?action=';
								if (developer['status'] == 'approved')
									status_url += "approve";
								else
									status_url += "revoke";

								request.post(status_url, function (error, response, body) {
									var status = 999;
									if (response)
										status = response.statusCode;
									grunt.verbose.writeln('Resp [' + status + '] for developer status ' + this.developer + ' - ' + this.developer + ' - ' + this.status_url + ' -> ' + body);
									if (error || status != 204)
										grunt.verbose.error('ERROR Resp [' + status + '] for ' + this.developer + ' - ' + this.developer + ' - ' + this.status_url + ' -> ' + body);

									// START of fix part1 issue #26
									status_done = true;
									if (delete_done) {
										done_count++;
										if (done_count == files.length) {
											grunt.log.ok('Processed ' + done_count + ' apps');
											done();
										}
										callback();
									}
									// END of fix part1 issue #26
								}.bind({ developer: developer, status_url: status_url, app_name: developer.email })).auth(userid, passwd, true);
							}
							// START of fix part 2 issue #26
							else {
								status_done = true;
								if (delete_done) {
									done_count++;
									if (done_count == files.length) {
										grunt.log.ok('Processed ' + done_count + ' developers');
										done();
									}
									callback();
								}
							}
							// END of fix part 2 issue #26
							// END of set App status

							// Delete the key generated when App is created
							/*var client_key = app_resp.credentials[0].consumerKey;
							var delete_url = app_url + '/' + app.name + '/keys/' + client_key;

							request.del(delete_url, function (error, response, body) {
								var status = 999;
								if (response)
									status = response.statusCode;
								grunt.verbose.writeln('Resp [' + status + '] for key delete ' + this.delete_url + ' -> ' + body);
								if (error || status != 200)
									grunt.log.error('ERROR Resp [' + status + '] for key delete ' + this.delete_url + ' -> ' + body);

								// START of fix part 3 issue #26
								delete_done = true;
								if (status_done) {
									done_count++;
									if (done_count == files.length) {
										grunt.log.ok('Processed ' + done_count + ' apps');
										done();
									}
									callback();
								}
								// END of fix part 3 issue #26					
							}.bind({ delete_url: delete_url })).auth(userid, passwd, true);*/
							// END of Key DELETE
						} else {
							grunt.verbose.writeln('ERROR Resp [' + response.statusCode + '] for create developer  ' + this.developer_url + ' -> ' + body);
							callback();
						}
					} catch (err) {
						grunt.log.error("ERROR - from App URL : " + developer_url);
						grunt.log.error(body);
					}
				}.bind({ developer_url: developer_url })).auth(userid, passwd, true);
		});
	});


	grunt.registerMultiTask('deleteApps', 'Delete all apps from org ' + apigee.to.org, function () {
		var url = apigee.to.url;
		var org = apigee.to.org;
		var userid = apigee.to.userid;
		var passwd = apigee.to.passwd;
		var done_count = 0;
		var files;
		url = url + "/v1/organizations/" + org + "/companies/";
		var done = this.async();
		var opts = { flatten: false };
		var f = grunt.option('src');
		if (f) {
			grunt.verbose.writeln('src pattern = ' + f);
			files = grunt.file.expand(opts, f);
		}
		else {
			files = this.filesSrc;
		}
		var done = this.async();
		files.forEach(function (filepath) {
			grunt.verbose.writeln("processing file " + filepath);
			var folders = filepath.split("/");
			var dev = folders[folders.length - 2];
			var content = grunt.file.read(filepath);
			var app = JSON.parse(content);
			var app_del_url = url + dev + "/apps/" + app.name;
			grunt.verbose.writeln(app_del_url);
			request.del(app_del_url, function (error, response, body) {
				var status = 999;
				if (response)
					status = response.statusCode;
				grunt.verbose.writeln('Resp [' + status + '] for delete app ' + this.app_del_url + ' -> ' + body);
				done_count++;
				if (error || status != 200)
					grunt.verbose.error('ERROR Resp [' + status + '] for delete app ' + this.app_del_url + ' -> ' + body);
				if (done_count == files.length) {
					grunt.log.ok('Processed ' + done_count + ' apps');
					done();
				}
			}.bind({ app_del_url: app_del_url })).auth(userid, passwd, true);
		});

	});

};


Array.prototype.unique = function () {
	var a = this.concat();
	for (var i = 0; i < a.length; ++i) {
		for (var j = i + 1; j < a.length; ++j) {
			if (a[i].name === a[j].name)
				a.splice(j--, 1);
		}
	}
	return a;
};