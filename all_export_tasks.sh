#!/bin/bash
grunt exportDevs -v
grunt exportProducts -v
grunt exportApps -v
grunt exportProxies -v
grunt exportSharedFlows -v
grunt exportProxyKVM -v
grunt exportEnvKVM -v
grunt exportOrgKVM -v
grunt exportFlowHooks -v
grunt exportTargetServers -v
grunt exportReports -v
grunt exportCompanies -v
grunt exportEnvCache -v
grunt exportUserRoles -v
grunt exportUserPermissions -v
grunt exportCompanyDevs -v
