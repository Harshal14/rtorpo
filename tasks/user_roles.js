/*jslint node: true */
var request = require('request');
var apigee = require('../config.js');
var userroles;
module.exports = function(grunt) {
	'use strict';
	grunt.registerTask('exportUserRoles', 'Export all user roles from org ' + apigee.from.org + " [" + apigee.from.version + "]", function() {
		var url = apigee.from.url;
		var org = apigee.from.org;
		var userid = apigee.from.userid;
		var passwd = apigee.from.passwd;
		var filepath = grunt.config.get("exportUserRoles.dest.data");
		var done_count =0;
		var done = this.async();
		grunt.verbose.writeln("========================= export User Roles ===========================" );

		grunt.verbose.writeln("getting user roles..." + url);
		url = url + "/v1/organizations/" + org + "/userroles";

		request(url, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				grunt.log.write("USER ROLES: " + body);
			    userroles =  JSON.parse(body);
			    
			    if( userroles.length == 0 ) {
			    	grunt.verbose.writeln("No User Roles");
			    	done();
			    }
			    for (var i = 0; i < userroles.length; i++) {
			    	var userroles_url = url + "/" + userroles[i];
			    	grunt.file.mkdir(filepath);

			    	//Call user roles details
			    	grunt.verbose.writeln("USER ROLES URL: " + userroles_url.length + " " + userroles_url);
			    	// An Edge bug allows user roles to be created with very long names which cannot be used in URLs.
			    	if( userroles_url.length > 1024 ) {
			    		grunt.log.write("SKIPPING User Roles, URL too long: ");
			    		done_count++;
			    	} else {
						request(userroles_url, function (error, response, body) {
							if (!error && response.statusCode == 200) {
								grunt.verbose.writeln("USER ROLES " + body);
							    var userroles_detail =  JSON.parse(body);
							    var dev_file = filepath + "/" + userroles_detail.name;
							    grunt.file.write(dev_file, body);

							    grunt.verbose.writeln('Exported User Roles ' + userroles_detail.name);
							}
							else
							{
								grunt.verbose.writeln('Error Exporting User Roles ' + userroles_detail.name);
								grunt.log.error(error);
							}

							done_count++;
							if (done_count == userroles.length)
							{
								grunt.log.ok('Processed ' + done_count + ' userroles');
	                            grunt.verbose.writeln("================== export userroles DONE()" );
								done();
							}
						}).auth(userid, passwd, true);
					}
			    	// End user roles details
			    };
			    
			} 
			else
			{
				grunt.log.error(error);
			}
		}).auth(userid, passwd, true);
		/*
		setTimeout(function() {
		    grunt.verbose.writeln("================== User Roles Timeout done" );
		    done(true);
		}, 10000);
		grunt.verbose.writeln("========================= export User Roles DONE ===========================" );
		*/
	});

	grunt.registerMultiTask('importUserRoles', 'Import all User roles  to org ' + apigee.to.org + " [" + apigee.to.version + "]", function() {
		var url = apigee.to.url;
		var org = apigee.to.org;
		var userid = apigee.to.userid;
		var passwd = apigee.to.passwd;
		var files = this.filesSrc;
		var done_count=0;
		var opts = {flatten: false};
		var f = grunt.option('src');
		if (f)
		{
			grunt.verbose.writeln('src pattern = ' + f);
			files = grunt.file.expand(opts,f);
		}
		url = url + "/v1/organizations/" + org + "/userroles";
		var done = this.async();

		files.forEach(function(filepath) {
			var content = grunt.file.read(filepath);
			//grunt.verbose.writeln(content);	
			request.post({
			  headers: {'content-type' : 'application/json'},
			  url:     url,
			  body:    content
			}, function(error, response, body){
			  var status = 999;
			  if (response)	
				status = response.statusCode;
			  grunt.verbose.writeln('Resp [' + status + '] for user roles creation ' + this.url + ' -> ' +body);
			  if (error || status!=201)
			  { 
			  	grunt.verbose.error('ERROR Resp [' + status + '] for user roles creation ' + this.url + ' -> ' +body); 
			  }
			 done_count++;
			if (done_count == files.length)
			{
				grunt.log.ok('Processed ' + done_count + ' userroles');
				done();
			}
			}.bind( {url: url}) ).auth(userid, passwd, true);

		});
	});

	grunt.registerMultiTask('deleteUserRoles', 'Delete all user roles from org ' + apigee.to.org + " [" + apigee.to.version + "]", function() {
		var url = apigee.to.url;
		var org = apigee.to.org;
		var userid = apigee.to.userid;
		var passwd = apigee.to.passwd;
		var files = this.filesSrc;
		var done_count = 0;
		var opts = {flatten: false};
		var f = grunt.option('src');
		if (f)
		{
			grunt.verbose.writeln('src pattern = ' + f);
			files = grunt.file.expand(opts,f);
		}
		url = url + "/v1/organizations/" + org + "/userroles/";
		var done = this.async();
		files.forEach(function(filepath) {
			var content = grunt.file.read(filepath);
			var userrole = JSON.parse(content);
			var del_url = url + userrole.name;
			grunt.verbose.writeln(del_url);	
			request.del(del_url, function(error, response, body){
			  var status = 999;
			  if (response)	
				status = response.statusCode;
			  grunt.verbose.writeln('Resp [' + status + '] for user role deletion ' + this.del_url + ' -> ' + body);
			  if (error || status!=200)
			  { 
			  	grunt.verbose.error('ERROR Resp [' + status + '] for user role deletion ' + this.del_url + ' -> ' + body); 
			  }
				done_count++;
				if (done_count == files.length)
				{
					grunt.log.ok('Processed ' + done_count + ' userroles');
					done();
				}
			}.bind( {del_url: del_url}) ).auth(userid, passwd, true);

		});
	});

};
