/*jslint node: true */
var request = require('request');
var apigee = require('../config.js');
var async = require('async');
var userpermissions;
module.exports = function(grunt) {
	'use strict';
	grunt.registerTask('exportUserPermissions', 'Export all user permissions from org ' + apigee.from.org + " [" + apigee.from.version + "]", function() {
		var url = apigee.from.url;
		var org = apigee.from.org;
		var userid = apigee.from.userid;
		var passwd = apigee.from.passwd;
		var filepath = grunt.config.get("exportUserPermissions.dest.data");
		var done_count =0;
		var userrole_count =0;
		var total_userpermissions =0;
		var userrole_url;
		var done = this.async();
		grunt.verbose.writeln("========================= export User Permissions ===========================" );
		grunt.verbose.writeln("getting userroles..." + url);
		url = url + "/v1/organizations/" + org + "/userroles";


		var dumpPermissions = function(userrole) {
			userrole_url = url + "/" + userrole;
			console.log("userrole_url is" + userrole_url);
			//Call userroles details
			request(userrole_url, function (userrole_error, userrole_response, userrole_body) {
				if (!userrole_error && userrole_response.statusCode == 200) {
					console.log("user role url is " + userrole_url);
					// grunt.verbose.write("Dev body = " + dev_body);
					var userrole_detail = JSON.parse(userrole_body);
					console.log("user role body is " + userrole_body);
					var last = userrole_detail.userrole;
					var userrole_folder = filepath + "/" + userrole_detail.name;
					grunt.file.mkdir(userrole_folder);
					//Get developer Apps
					var userpermission_url = url + "/" + userrole_detail.name + "/users";
					console.log("userpermission url is" + userpermission_url);
					grunt.verbose.writeln(userpermission_url);
					request(userpermission_url, function (userpermission_error, userpermission_response, userpermission_body) {
						if (!userpermission_error && userpermission_response.statusCode == 200) {
                            console.log("Inside dump");
                            var userpermissions_detail = JSON.parse(userpermission_body);
                            console.log("Inside dump1 " + userpermission_body);
							grunt.verbose.writeln(userpermission_body);							
							//var userpermissions = JSON.parse(userpermissions_detail);
							
							/*for(var resourcePermission in userpermissions_detail){
								console.log("items are "+ resourcePermission);
								for (var j = 0; j < resourcePermission.length; j++) {
									var userpermission = resourcePermission[j];
									grunt.verbose.writeln(JSON.stringify(userpermission));

                                    //var file_name  = dev_folder + "/" + app.appId;
                                    //made changes here
									var file_name = userrole_folder + "/" + userpermission.name;
									console.log("file name to write 1 " + userrole_folder);
									console.log("file name to write is " + file_name);
									grunt.verbose.writeln("writing file: " + file_name);
									grunt.file.write(file_name, JSON.stringify(userpermission));
									grunt.verbose.writeln('UserPermission ' + userpermission.name + ' written!');
								};
								
							}
							console.log("dump23"+ userpermissions); */
                            //console.log("dump22 " + userpermissions);
							//grunt.verbose.writeln(apps);
							if (userpermissions) {
								for (var j = 0; j < userpermissions.length; j++) {
									var userpermission = userpermissions[j];
									grunt.verbose.writeln(JSON.stringify(userpermission));
                                    //var file_name  = dev_folder + "/" + app.appId;
                                    //made changes here
									var file_name = userrole_folder + "/" + userpermission.name;
									grunt.verbose.writeln("writing file: " + file_name);
									grunt.file.write(file_name, JSON.stringify(userpermission));
									grunt.verbose.writeln('UserPermission ' + userpermission.name + ' written!');
								};
							}
							total_userpermissions += userpermissions.length;

							if (userpermissions.length > 0) {
								grunt.log.ok('Retrieved ' + userpermissions.length + ' userpermissions for ' + userrole_detail.userrole);
							} else {
								grunt.verbose.writeln('Retrieved ' + userpermissions.length + ' userpermissions for ' + userrole_detail.userrole);
							}
						}
						else {
							grunt.verbose.writeln('Error Exporting ' + userpermission.name);
							grunt.log.error(body);
						}
						done_count++;
						if (done_count == userrole_count) {
							grunt.log.ok('Exported ' + total_userpermissions + ' user permissions for ' + done_count + ' userroles');
                            grunt.verbose.writeln("================== export user permissions DONE()" );
							done();
						}
					}).auth(userid, passwd, true);
				}
				else {
					if (userrole_error )
						grunt.log.error(userrole_error);
					else
						grunt.log.error(userrole_body);
				}
			}).auth(userid, passwd, true);
			// End User Role details
		}

		var iterateOverUserRoles = function(start, base_url, callback) {
			var url = base_url;

			if (start) {
				url += "?startKey=" + encodeURIComponent(start);
			}
			grunt.verbose.writeln("getting userroles..." + url);

			request(url, function (error, response, body) {
                grunt.log.ok("value of body is "+body);
                console.log("value of body is "+body);
				if (!error && response.statusCode == 200) {
                    console.log("Inside 1 ");
					var userroles = JSON.parse(body);
					var last = null;
                    console.log("Inside 11 "+userroles.length);
					// detect none or that the only developer returned is the one we asked to start with; that's the end game.
					if ((userroles.length == 0) || (userroles.length == 1 && userroles[0] == start) ) {
                        console.log("Inside 2 " +userroles.length);
						grunt.log.ok('Retrieved total of ' + userrole_count + ' userroles');
						done();
					} else {
						userrole_count += userroles.length;
						if (start)
                        userrole_count--;

						for (var i = 0; i < userroles.length; i++) {
                            // If there was a 'start', don't do it again, because it was processed in the previous callback.
                            console.log("Inside 3 " +userroles.length);
							if (!start || userroles[i] != start) {
								callback(userroles[i]);
								last = userroles[i];
							}
						}

						grunt.log.ok('Retrieved ' + userroles.length + ' userpermissions');

						// Keep on calling getDevs() as long as we're getting new developers back
						iterateOverUserRoles(last, base_url, callback);
					}
				}
				else {
					if (error)
						grunt.log.error(error);
					else
						grunt.log.error(body);
				}

			}).auth(userid, passwd, true);
		}

		iterateOverUserRoles(null, url, dumpPermissions);
		/*
		setTimeout(function() {
		    grunt.verbose.writeln("================== Apps Timeout done" );
		    done(true);
		}, 3000);
		grunt.verbose.writeln("========================= export Apps DONE ===========================" );
		*/
	});

	grunt.registerMultiTask('importApps', 'Import all apps to org ' + apigee.to.org + " [" + apigee.to.version + "]", function() {
	    var url = apigee.to.url;
	    var org = apigee.to.org;
	    var userid = apigee.to.userid;
	    var passwd = apigee.to.passwd;
	    var done_count = 0;
	    var files;
	    url = url + "/v1/organizations/" + org + "/developers/";
	    var opts = {
	        flatten: false
	    };
	    var f = grunt.option('src');
	    if(f) {
	        grunt.verbose.writeln('src pattern = ' + f);
	        files = grunt.file.expand(opts, f);
	    } else {
	        files = this.filesSrc;
	    }

	    var done = this.async();

	    async.eachSeries(files, function(filepath, callback) {
	        console.log(filepath);
	        var folders = filepath.split("/");
	        var dev = folders[folders.length - 2];
	        var content = grunt.file.read(filepath);
	        var app = JSON.parse(content);
	        grunt.verbose.writeln("Creating app : " + app.name + " under developer " + dev);

	        var status_done = false;
	        var delete_done = false;

	        delete app['appId'];
	        //delete app['status'];
	        delete app['developerId'];
	        delete app['lastModifiedAt'];
	        delete app['lastModifiedBy'];
	        delete app['createdAt'];
	        delete app['createdBy'];
	        //delete app['status'];
	        delete app['appFamily'];
	        delete app['accessType'];
	        delete app['credentials'];

	        grunt.verbose.writeln(JSON.stringify(app));
	        var app_url = url + dev + "/apps";
	        grunt.verbose.writeln("Creating App " + app_url);

	        request.post({
	                headers: {
	                    'Content-Type': 'application/json'
	                },
	                url: app_url,
	                body: JSON.stringify(app)
	            },
	            function(error, response, body) {
	                try {
	                    var cstatus = 999;
	                    if(response)
	                        cstatus = response.statusCode;
	                    if(cstatus == 200 || cstatus == 201) {
	                        grunt.verbose.writeln('Resp [' + response.statusCode + '] for create app  ' + this.app_url + ' -> ' + body);
	                        var app_resp = JSON.parse(body);

	                       	// Set app status
	                        if (app['status'] && (app_resp['status'] != app['status'])) {
	                            var status_url = app_url + '/' + app.name + '?action=';
	                            if(app['status'] == 'approved')
	                                status_url += "approve";
	                            else
	                                status_url += "revoke";

	                            request.post(status_url, function(error, response, body) {
	                                var status = 999;
	                                if(response)
	                                    status = response.statusCode;
	                                grunt.verbose.writeln('Resp [' + status + '] for app status ' + this.dev + ' - ' + this.app_name + ' - ' + this.status_url + ' -> ' + body);
	                                if(error || status != 204)
	                                    grunt.verbose.error('ERROR Resp [' + status + '] for ' + this.dev + ' - ' + this.app_name + ' - ' + this.status_url + ' -> ' + body);

	                                // START of fix part1 issue #26
	                                status_done = true;
	                                if(delete_done) {
	                                    done_count++;
	                                    if(done_count == files.length) {
	                                        grunt.log.ok('Processed ' + done_count + ' apps');
	                                        done();
	                                    }
	                                    callback();
	                                }
	                                // END of fix part1 issue #26
	                            }.bind({dev: dev,status_url: status_url,app_name: app.name})).auth(userid, passwd, true);
	                        }
	                        // START of fix part 2 issue #26
	                        else {
	                            status_done = true;
	                            if(delete_done) {
	                                done_count++;
	                                if(done_count == files.length) {
	                                    grunt.log.ok('Processed ' + done_count + ' apps');
	                                    done();
	                                }
	                                callback();
	                            }
	                        }
	                        // END of fix part 2 issue #26
	                        // END of set App status

	                        // Delete the key generated when App is created
	                        var client_key = app_resp.credentials[0].consumerKey;
	                        var delete_url = app_url + '/' + app.name + '/keys/' + client_key;

	                        request.del(delete_url, function(error, response, body) {
	                            var status = 999;
	                            if(response)
	                                status = response.statusCode;
	                            grunt.verbose.writeln('Resp [' + status + '] for key delete ' + this.delete_url + ' -> ' + body);
	                            if(error || status != 200)
	                                grunt.log.error('ERROR Resp [' + status + '] for key delete ' + this.delete_url + ' -> ' + body);

	                            // START of fix part 3 issue #26
	                            delete_done = true;
	                            if(status_done) {
	                                done_count++;
	                                if(done_count == files.length) {
	                                    grunt.log.ok('Processed ' + done_count + ' apps');
	                                    done();
	                                }
	                                callback();
	                            }
	                            // END of fix part 3 issue #26					
	                        }.bind({delete_url: delete_url})).auth(userid, passwd, true);
	                        // END of Key DELETE
	                    } else {
	                        grunt.verbose.writeln('ERROR Resp [' + response.statusCode + '] for create app  ' + this.app_url + ' -> ' + body);
	                        callback();
	                    }
	                } catch (err) {
	                    grunt.log.error("ERROR - from App URL : " + app_url);
	                    grunt.log.error(body);
	                }
	            }.bind({app_url: app_url})).auth(userid, passwd, true);
	    });
	});


	grunt.registerMultiTask('deleteApps', 'Delete all apps from org ' + apigee.to.org + " [" + apigee.to.version + "]", function() {
		var url = apigee.to.url;
		var org = apigee.to.org;
		var userid = apigee.to.userid;
		var passwd = apigee.to.passwd;
		var done_count =0;
		var files;
		url = url + "/v1/organizations/" + org + "/developers/";
		var done = this.async();
		var opts = {flatten: false};
		var f = grunt.option('src');
		if (f)
		{
			grunt.verbose.writeln('src pattern = ' + f);
			files = grunt.file.expand(opts,f);
		}
		else
		{
			files = this.filesSrc;
		}
		var done = this.async();
		files.forEach(function(filepath) {
			grunt.verbose.writeln("processing file " + filepath);
			var folders = filepath.split("/");
			var dev = folders[folders.length - 2];
			var content = grunt.file.read(filepath);
			var app = JSON.parse(content);
			var app_del_url = url + dev + "/apps/" + app.name;
			grunt.verbose.writeln(app_del_url);
			request.del(app_del_url,function(error, response, body){
			   var status = 999;
			   if (response)	
			    status = response.statusCode;
			  grunt.verbose.writeln('Resp [' + status + '] for delete app ' + this.app_del_url + ' -> ' + body);
			  done_count++;
			  if (error || status!=200)
			  	grunt.verbose.error('ERROR Resp [' + status + '] for delete app ' + this.app_del_url + ' -> ' + body); 
			  if (done_count == files.length)
			  {
				grunt.log.ok('Processed ' + done_count + ' apps');
				done();
			  }
			}.bind( {app_del_url: app_del_url}) ).auth(userid, passwd, true);	
		});

	});

};

Array.prototype.unique = function() {
    var a = this.concat();
    for(var i=0; i<a.length; ++i) {
        for(var j=i+1; j<a.length; ++j) {
            if(a[i].name === a[j].name)
                a.splice(j--, 1);
        }
    }
    return a;
};
