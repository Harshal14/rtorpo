/*jslint node: true */
var request = require('request');
var apigee = require('../config.js');
var roles;
module.exports = function(grunt) {
	'use strict';
	grunt.registerTask('exportRoles', 'Export all roles from org ' + apigee.from.org + " [" + apigee.from.version + "]", function() {
		var url = apigee.from.url;
		var org = apigee.from.org;
		var userid = apigee.from.userid;
		var passwd = apigee.from.passwd;
		var filepath = grunt.config.get("exportRoles.dest.data");
		var done_count =0;
		var done = this.async();
		grunt.verbose.writeln("========================= export Roles ===========================" );

		grunt.verbose.writeln("getting roles..." + url);
		url = url + "/v1/organizations/" + org + "/userroles";

		request(url, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				grunt.log.write("ROLES: " + body);
			    roles =  JSON.parse(body);			    
			    if( roles.length == 0 ) {
			    	grunt.verbose.writeln("No Roles");
			    	done();
			    }
			    for (var i = 0; i < roles.length; i++) {
			    	var roles_url = url + "/" + roles[i];
			    	grunt.file.mkdir(filepath);

			    	//Call roles details
			    	grunt.verbose.writeln("ROLES URL: " + roles_url.length + " " + roles_url);			    	
			    	if( roles_url.length > 1024 ) {
			    		grunt.log.write("SKIPPING Roles, URL too long: ");
			    		done_count++;
			    	} else {
						request(roles_url, function (error, response, body) {
							if (!error && response.statusCode == 200) {
								grunt.verbose.writeln("ROLES " + body);
							    var roles_detail =  JSON.parse(body);
								var role_file = filepath + "/" + roles_detail.name;								
								if(roles_detail.name != 'devadmin' && roles_detail.name != null){
							    	grunt.file.write(role_file, body);
									grunt.verbose.writeln('Exported Roles ' + roles_detail.name);
								}
							}
							else
							{								
								grunt.log.error(error);
							}

							done_count++;
							if (done_count == roles.length)
							{
								grunt.log.ok('Processed ' + done_count + ' roles');
	                            grunt.verbose.writeln("================== export roles DONE()" );
								done();
							}
						}).auth(userid, passwd, true);
					}
			    	// End roles details
			    };
			    
			} 
			else
			{
				grunt.log.error(error);
			}
		}).auth(userid, passwd, true);
	});

	grunt.registerMultiTask('importRoles', 'Import all roles  to org ' + apigee.to.org + " [" + apigee.to.version + "]", function() {
		var url = apigee.to.url;
		var org = apigee.to.org;
		var userid = apigee.to.userid;
		var passwd = apigee.to.passwd;
		var files = this.filesSrc;
		var done_count=0;
		var opts = {flatten: false};
		var f = grunt.option('src');
		if (f)
		{
			grunt.verbose.writeln('src pattern = ' + f);
			files = grunt.file.expand(opts,f);
		}
		url = url + "/v1/organizations/" + org + "/userroles";
		var done = this.async();

		files.forEach(function(filepath) {
			var content = grunt.file.read(filepath);
			//grunt.verbose.writeln(content);	
			request.post({
			  headers: {'content-type' : 'application/json'},
			  url:     url,
			  body:    content
			}, function(error, response, body){
			  var status = 999;
			  if (response)	
				status = response.statusCode;
			  grunt.verbose.writeln('Resp [' + status + '] for roles creation ' + this.url + ' -> ' +body);
			  if (error || status!=201)
			  { 
			  	grunt.verbose.error('ERROR Resp [' + status + '] for roles creation ' + this.url + ' -> ' +body); 
			  }
			 done_count++;
			if (done_count == files.length)
			{
				grunt.log.ok('Processed ' + done_count + ' roles');
				done();
			}
			}.bind( {url: url}) ).auth(userid, passwd, true);

		});
	});

	grunt.registerMultiTask('deleteRoles', 'Delete all roles from org ' + apigee.to.org + " [" + apigee.to.version + "]", function() {
		var url = apigee.to.url;
		var org = apigee.to.org;
		var userid = apigee.to.userid;
		var passwd = apigee.to.passwd;
		var files = this.filesSrc;
		var done_count = 0;
		var opts = {flatten: false};
		var f = grunt.option('src');
		if (f)
		{
			grunt.verbose.writeln('src pattern = ' + f);
			files = grunt.file.expand(opts,f);
		}
		url = url + "/v1/organizations/" + org + "/userroles/";
		var done = this.async();
		files.forEach(function(filepath) {
			var content = grunt.file.read(filepath);
			var role = JSON.parse(content);
			var del_url = url + role.name;
			grunt.verbose.writeln(del_url);	
			request.del(del_url, function(error, response, body){
			  var status = 999;
			  if (response)	
				status = response.statusCode;
			  grunt.verbose.writeln('Resp [' + status + '] for role deletion ' + this.del_url + ' -> ' + body);
			  if (error || status!=200)
			  { 
			  	grunt.verbose.error('ERROR Resp [' + status + '] for role deletion ' + this.del_url + ' -> ' + body); 
			  }
				done_count++;
				if (done_count == files.length)
				{
					grunt.log.ok('Processed ' + done_count + ' roles');
					done();
				}
			}.bind( {del_url: del_url}) ).auth(userid, passwd, true);

		});
	});

};
